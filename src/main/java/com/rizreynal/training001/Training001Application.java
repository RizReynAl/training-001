package com.rizreynal.training001;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Training001Application {

	public static void main(String[] args) {
		SpringApplication.run(Training001Application.class, args);
	}

}
