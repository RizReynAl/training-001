package com.rizreynal.training001.controller;

import com.rizreynal.training001.model.User;
import com.rizreynal.training001.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

/**
 * Created by rizkir on 22/03/2019.
 */

@RestController
public class UserController {

    @Autowired
    private UserRepository userRepository;

    @GetMapping("/test")
    public String testCall(){
        return "Test Calling";
    }


    @GetMapping("/users")
    public List<User> getAllUser(){
        return this.userRepository.findAll();
    }

    @PostMapping("/user")
    public  String addUser(@RequestBody User user){
        User s =  this.userRepository.save(user);
        if (!Objects.isNull(s))
            return "Success Add : "+s.getNamaUser();
        else
            return "Something Wrong!";
    }

    @DeleteMapping("/user/{id}")
    public String delUser(@PathVariable("id") Long id){
        User user = this.userRepository.getOne(id);
        if (!Objects.isNull(user)){
            this.userRepository.delete(user);
            return "berhasil";
        }else
            return "Error 530 : Data yang dimaksud Tidak Ditemukan";
    }



    @PutMapping("/user/{id}")
    public String updateUser(@PathVariable("id") Long id,
                             @RequestBody User user){

        if (!Objects.isNull(this.userRepository.getOne(id))){
            user.setId(id);
            this.userRepository.save(user);
            return "berhasil Update  ";
        }else
            return "Error 530 : Data yang dimaksud Tidak Ditemukan";
    }

}
