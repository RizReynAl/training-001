package com.rizreynal.training001.repository;

import com.rizreynal.training001.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by rizkir on 22/03/2019.
 */

@Repository
public interface UserRepository extends JpaRepository<User,Long>{

}
