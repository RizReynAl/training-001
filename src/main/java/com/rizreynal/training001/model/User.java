package com.rizreynal.training001.model;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

/**
 * Created by rizkir on 22/03/2019.
 */

@Entity
@Table(name="tb_user")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id_user",updatable = false, nullable = false)
    Long id;

    @Size(min = 5, max = 250)
    @Column(name="nama_user")
    String namaUser;

    @NotBlank
    @Column(name="email_user")
    String emailUser;

    public User() {
    }

    public User( String namaUser, String emailUser) {
        this.namaUser = namaUser;
        this.emailUser = emailUser;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNamaUser() {
        return namaUser;
    }

    public void setNamaUser(String namaUser) {
        this.namaUser = namaUser;
    }

    public String getEmailUser() {
        return emailUser;
    }

    public void setEmailUser(String emailUser) {
        this.emailUser = emailUser;
    }
}
