package com.rizreynal.training001;

import com.rizreynal.training001.model.User;
import com.rizreynal.training001.repository.UserRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class Training001ApplicationTests {

	@Autowired
	UserRepository userRepository;

	@Test
	public void contextLoads() {
//		User user = new User("Rizki Reynaldi","reynaldirizki43@gmail.com");
//		userRepository.save(user);

//		Syntax for update record .. via JpaRepository
		User user1 = userRepository.findById(Long.valueOf(1)).get();
		user1.setEmailUser("Aldi@gmail.com");
		user1.setNamaUser("Aldy reynaldi");
		userRepository.save(user1);
	}

}
